class Post < ActiveRecord::Base
    has_many :comment
    validates :title, presence: true
    validates :body, presence: true
end